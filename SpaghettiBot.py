from discord.ext import commands
import json


with open('config/config.json') as config_file:
    config = json.load(config_file)

bot = commands.Bot(command_prefix=config["spaghetti_prefix"])
startup_extensions = ["cogs.Extensions", "cogs.Basic", "cogs.Luc"]


@bot.event
async def on_ready():
    print("\n## Logged in as", bot.user.name)
    print("## ID:", bot.user.id)
    print('')
    if __name__ == "__main__":
        for extension in startup_extensions:
            try:
                bot.load_extension(extension)
            except Exception as e:
                exc = '{}: {}'.format(type(e).__name__, e)
                print(exc)


bot.run(config["tokens"]["SpaghettiBot"])
