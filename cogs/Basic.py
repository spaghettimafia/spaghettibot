import discord
from discord.ext import commands
import asyncio


class Basic(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.urgent_loop = None
        self.urgent_user = None

    @commands.command()
    async def say(self, ctx, *, msg):
        await ctx.send(msg)

    @commands.command()
    async def spaghetti(self, ctx):
        await ctx.send(":spaghetti: ")

    @commands.command()
    async def party(self, ctx):
        await ctx.send(ctx.guild.emojis)

    @commands.command()
    async def urgent(self, ctx, user: discord.User):
        self.urgent_loop = True
        self.urgent_user = user
        for x in range(0, 10):
            if self.urgent_loop:
                try:
                    await tag.delete()
                except:
                    pass
                tag = await ctx.send("<@{}>, `{}` has a message for you!".format(user.id, ctx.author.user.name))
                await asyncio.sleep(120)

    @commands.Cog.listener()
    async def on_message(self, message):
        if self.urgent_user is not None:
            if message.author.id == self.urgent_user.id:
                self.urgent_loop = False

def setup(bot):
    bot.add_cog(Basic(bot))
