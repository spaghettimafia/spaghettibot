import discord
from discord.ext import commands
import asyncio


class Luc(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message):
        pass
        #print(message.reactions)
        #await message.add_reaction()

    @commands.command()
    async def test(self, ctx):
        msg = await ctx.send("React on this message!")
        await asyncio.sleep(5)
        print(msg.reactions)

    @commands.command(usage="<@user>")
    async def pict(self, ctx, user: discord.User):
        await ctx.send(user.avatar_url)


def setup(bot):
    bot.add_cog(Luc(bot))